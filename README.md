# Patience
Patience executes `$command` every `$interval` seconds for `$timeout` seconds.  It then prints `$message`.


## Usage
```sh
Usage: foo.sh "command" [interval] [timeout] ["message"]

Examples:
	foo.sh "false" 1 3
	foo.sh "docker exec -it foobar bash" 1 5 "fail whale"

Defaults:
	command		- <empty (required)>
	internval	- 5
	timeout		- 30
	message		- "timeout"
```

## Installation
Nothing is required to "install" Patience, as it is simply a Bash script.

That said, one may wish to have it more conveniently available throughout their system, e.g., globally.

Assuming that `~/bin` is in your `$PATH`, here is one way to accomplish this:
```sh
$ ln -s [path to here]/patience.sh ~/bin/patience
$ patience false
Patience starting (every 5s for 30s)...
...timeout (30s / 30s)
```
