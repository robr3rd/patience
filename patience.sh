#! /usr/bin/env bash
#
# Patience.sh
#
# Patience executes `$command` every `$interval` seconds for `$timeout` seconds.  It then prints `$message`.
#

# --- Help text ---
read -r -d '' usagestring <<USAGESTRING
Usage: $(basename "$0") "command" [interval] [timeout] ["message"]

Examples:
	$(basename "$0") "false" 1 3
	$(basename "$0") "docker exec -it foobar bash" 1 5 "fail whale"

Defaults:
	command		- <empty (required)>
	internval	- 5
	timeout		- 30
	message		- "timeout"
USAGESTRING

function usage {
	echo "$usagestring"
	# shellcheck disable=2086
	exit $1 # forward the exit code passed to this function (arg1)
}

if [ "$1" == '' ] || [ "$1" == 'help' ]; then
	usage 1
fi



# --- Begin script ---
command="$1"
interval=${2:-5}
timeout=${3:-30}
message="${4:-timeout}"

echo "Patience starting (every ${interval}s for ${timeout}s)..."

start_time=$(date +%s)
while ( ! eval "$command" ); do
  total_time=$(($(date +%s) - $start_time))

  if [ $total_time -ge $timeout ]; then
    echo "...${message} (${total_time}s / ${timeout}s)"
    exit 1
  else
    sleep $interval
  fi
done

exit 0
